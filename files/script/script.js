/**
 * seznam původních todo
 */
const initialTodos = [
    { title: 'Najít prvek div#todos', completed: true },
    { title: 'Vytvořit vstupní pole pro text' },
    { title: 'Vytvořit seznam UL' },
    { title: 'Po odentrování, pokud je text > 3 znaky, vytvořit TODO' },
    { title: 'vytvořit fci na TODO, closure' },
    { title: 'Po kliknutí na X přepnout stav na dokončen a naopak' },
    { title: 'Uvařit večeři a jít spát' },
];

/**
 * Single todo item
 */
class TodoItem {
    completed = false
    title = ""
    todo = null

    constructor({ title, completed = false }) {
        if (typeof title != 'string' || typeof completed != 'boolean') {
            throw new Error("Chybný typ");
        }

        this.title = title;
        this.completed = completed;
    }

    /**
     * Completes the Todo
     */
    complete() {
        this.completed = true;
    }

    /**
     * Reset the Todo state if needed
     */
    reset() {
        this.completed = false;
    }
    /**
     * vytvoříme todolist
     * 
     * @param {*} todoList
     * 
     * @return Todo element
     */
    render(todoList) {
        if (todoList.appendChild == undefined) {
            throw new Error('Špatný rodičovský prvek');
        }

        // vytvořím todo prvek
        const todo = document.createElement('li');
        todo.innerText = this.title;
        todo.className = 'todo';

        if (this.completed) {
            todo.className = 'completed';
        }

        // Link Todo element to the object
        this.todo = todo;

        const todoCheckmark = document.createElement('span');
        todoCheckmark.innerText = this.completed ? "✓" : "x";

        // change the state if clicked Todo
        todo.addEventListener('click', () => {
            todoCheckmark.innerText = this.completed ? "x" : "✓";
            this.todo.classList.toggle('completed');

            this.completed = !this.completed;
        })

        // Add checkmark element to the todoItem
        todo.appendChild(todoCheckmark);
        // Add item to TodoList
        todoList.appendChild(this.todo);
    }
}

// Find div#todos
const todos = document.getElementById("todos");

if (todos == null) {
    throw Error("Todos is null");
}

const inputText = document.createElement('input');
inputText.type = 'text';
inputText.placeholder = 'Zadej text';
inputText.className = 'todo-input';

todos.appendChild(inputText);

// when entered text, add it to the list
inputText.addEventListener('keyup', (e) => {
    const value = e.target.value;
    if (value.length > 3 && e.key == 'Enter'){
        const item = new TodoItem({
            title: value
        });

        item.render(todos);
    }
});

// Go through the list of Todos and render it to the list
initialTodos.map((item) => {
    const Todo = new TodoItem(item);
    Todo.render(todos);
});